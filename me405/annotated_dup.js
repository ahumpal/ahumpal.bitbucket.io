var annotated_dup =
[
    [ "encoder_driver", null, [
      [ "encoder", "classencoder__driver_1_1encoder.html", "classencoder__driver_1_1encoder" ]
    ] ],
    [ "encoder_driveru", null, [
      [ "encoder", "classencoder__driveru_1_1encoder.html", "classencoder__driveru_1_1encoder" ]
    ] ],
    [ "mcp9808", null, [
      [ "tempsensor", "classmcp9808_1_1tempsensor.html", "classmcp9808_1_1tempsensor" ]
    ] ],
    [ "motor_driver", null, [
      [ "MotorDriver", "classmotor__driver_1_1MotorDriver.html", "classmotor__driver_1_1MotorDriver" ]
    ] ],
    [ "motor_driveru", null, [
      [ "MotorDriver", "classmotor__driveru_1_1MotorDriver.html", "classmotor__driveru_1_1MotorDriver" ]
    ] ],
    [ "screendriver", null, [
      [ "touchscreen", "classscreendriver_1_1touchscreen.html", "classscreendriver_1_1touchscreen" ]
    ] ],
    [ "screendriveru", null, [
      [ "touchscreen", "classscreendriveru_1_1touchscreen.html", "classscreendriveru_1_1touchscreen" ]
    ] ]
];