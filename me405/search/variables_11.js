var searchData=
[
  ['x_330',['x',['../frontend_8py.html#a7dfb2aaab1d178106b33b99b3f2a5a96',1,'frontend.x()'],['../lab9main_8py.html#a2916f952311f91fc3e38907a273382ee',1,'lab9main.x()'],['../T__F__Test_8py.html#abb556ca7a537b8b2a543ca19ced75f44',1,'T_F_Test.x()']]],
  ['x_5fdot_331',['x_dot',['../lab9main_8py.html#aeec8a1f48899eaee9eccf5822172c109',1,'lab9main']]],
  ['x_5fnew_332',['x_new',['../lab9main_8py.html#acaaaf409cb41a322388308261c62cd22',1,'lab9main']]],
  ['xbegin_333',['xbegin',['../screendriver_8py.html#a4ee8fd15c4b7717ea41b968fd8aec2d1',1,'screendriver.xbegin()'],['../screendriveru_8py.html#a43a040ac2d01014993dbda9cad6c187d',1,'screendriveru.xbegin()']]],
  ['xcord_334',['xcord',['../screendriver_8py.html#aef1121e955c7388b3bc3ff790c629a60',1,'screendriver.xcord()'],['../screendriveru_8py.html#aaf71dfd84c31647194a66394892bc78e',1,'screendriveru.xcord()']]],
  ['xm_335',['xm',['../screendriver_8py.html#a8499f436590d5be040bb379f09f5eaf7',1,'screendriver.xm()'],['../screendriveru_8py.html#aac20436f54e5509c2ddc7a4e73a57e8c',1,'screendriveru.xm()']]],
  ['xnew_336',['xnew',['../screendriver_8py.html#a8a9bf573f656061ce3609bbf10dc3550',1,'screendriver.xnew()'],['../screendriveru_8py.html#ab7c7c8c8b9da7615d6ae0bd1a41c5168',1,'screendriveru.xnew()']]],
  ['xnow_337',['xnow',['../screendriver_8py.html#afbb0b90f30c8b62e6bd912cc66b4704a',1,'screendriver.xnow()'],['../screendriveru_8py.html#ae1511d67d33903722220ca00003eb954',1,'screendriveru.xnow()']]],
  ['xorigin_338',['xorigin',['../screendriver_8py.html#aa0def2defe35be9b35f670f71edcabee',1,'screendriver.xorigin()'],['../screendriveru_8py.html#a0dd51d441e476afb8427ab79b2be9880',1,'screendriveru.xorigin()']]],
  ['xp_339',['xp',['../screendriver_8py.html#ac22d96cf04bd61c8889987f500ab2a1c',1,'screendriver.xp()'],['../screendriveru_8py.html#a4b94cd0da08868ac58b3ee1b7329c7bb',1,'screendriveru.xp()']]]
];
