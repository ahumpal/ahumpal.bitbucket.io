/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ashley Humpal", "index.html", [
    [ "Portfolio Details", "index.html#sec_port", null ],
    [ "Lab01 - Vendotron", "index.html#sec_lab1", null ],
    [ "Lab02 - Think Fast", "index.html#sec_lab2", null ],
    [ "Lab03 - Pushing the Right Buttons", "index.html#sec_lab3", null ],
    [ "Lab04 - Hot or Not?", "index.html#sec_lab4", null ],
    [ "Lab05 - Feeling Tipsy?", "index.html#sec_lab5", null ],
    [ "Lab06 - Simulation or Reality?", "index.html#sec_lab6", null ],
    [ "Lab07 - Feeling Touchy?", "index.html#sec_lab7", null ],
    [ "Lab08 - Term Project Part 1", "index.html#sec_lab8", null ],
    [ "Lab09 - Term Project Part 2", "index.html#sec_lab9", null ],
    [ "Lab05Handcalcs", "Lab05Handcalcs.html", [
      [ "Lab05 Hand Calculations", "Lab05Handcalcs.html#sec_lab5handcalc", null ]
    ] ],
    [ "Lab06Graphs", "Lab06Graphs.html", [
      [ "Lab06 Graphs", "Lab06Graphs.html#sec_lab6handcalc", null ]
    ] ],
    [ "Lab09Analytics", "Lab09Analytics.html", [
      [ "Lab09 Analytical Gains", "Lab09Analytics.html#sec_lab9analytics", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Lab05Handcalcs.html",
"screendriver_8py.html#a4ee8fd15c4b7717ea41b968fd8aec2d1"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';