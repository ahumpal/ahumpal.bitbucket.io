var frontend_8py =
[
    [ "receiveChar", "frontend_8py.html#a29ab9ad8c978d6a91da4c89894235593", null ],
    [ "sendChar", "frontend_8py.html#a966d39222cb5886d5866dbfb8e4cf329", null ],
    [ "ADClist", "frontend_8py.html#a3daaf18e9bdcd4d8b5845f4057872057", null ],
    [ "data", "frontend_8py.html#a99c03e7834eb7b359fa8378cffc0c5e7", null ],
    [ "delimiter", "frontend_8py.html#a8db469ee918963c381da1ecbae66adff", null ],
    [ "inv", "frontend_8py.html#a3c6a20cfcc7a746277225ed43f68841d", null ],
    [ "line_list", "frontend_8py.html#a15ed1b4b36d51449943dff24b3ebcf5f", null ],
    [ "ser", "frontend_8py.html#ac1339dd0c83bfba62f31b23e81b67b00", null ],
    [ "TIME", "frontend_8py.html#a236c5a830cbf58e16a155f998c507d8c", null ],
    [ "TIMEs", "frontend_8py.html#ab7fd8b415722757a7394f9948ad9d358", null ],
    [ "VOLT", "frontend_8py.html#ad76834016f0976d062a7be7d8cff539f", null ],
    [ "VOLTs", "frontend_8py.html#ae2742694966a18aed98f5e2d81deb12e", null ],
    [ "x", "frontend_8py.html#a7dfb2aaab1d178106b33b99b3f2a5a96", null ]
];