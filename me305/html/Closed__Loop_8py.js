var Closed__Loop_8py =
[
    [ "ClosedLoop", "classClosed__Loop_1_1ClosedLoop.html", "classClosed__Loop_1_1ClosedLoop" ],
    [ "get_Kp", "Closed__Loop_8py.html#a2f2110c2e63fe64962fe53243a7ef48e", null ],
    [ "set_Kp", "Closed__Loop_8py.html#a762fd62cb75137cf7a00a3b7005e8e3d", null ],
    [ "update", "Closed__Loop_8py.html#a77c3a72a06d8b57ccbaef2391fe9b369", null ],
    [ "Kp", "Closed__Loop_8py.html#a3a36a430548abe88adb789b580964389", null ],
    [ "kpprime", "Closed__Loop_8py.html#a1c0b2ef5c479607657fd0c6843786736", null ],
    [ "L", "Closed__Loop_8py.html#a9658c464922d86195b42816c59acc684", null ],
    [ "omegaact", "Closed__Loop_8py.html#a12b369c5a778f9f45e6e76db6888764a", null ],
    [ "omegaref", "Closed__Loop_8py.html#ab21d34bc7adbe44e024498a0a546312b", null ],
    [ "vm", "Closed__Loop_8py.html#ac98b60b8a3083ff410a74af1df73998b", null ]
];