var annotated_dup =
[
    [ "Bluetooth_Module_Driver", null, [
      [ "Bluetooth", "classBluetooth__Module__Driver_1_1Bluetooth.html", "classBluetooth__Module__Driver_1_1Bluetooth" ]
    ] ],
    [ "Bluetoothwo", null, [
      [ "Frequency", "classBluetoothwo_1_1Frequency.html", null ]
    ] ],
    [ "CLbackend", null, [
      [ "Backend", "classCLbackend_1_1Backend.html", null ]
    ] ],
    [ "CLbackend7", null, [
      [ "Backend", "classCLbackend7_1_1Backend.html", null ]
    ] ],
    [ "Closed_Loop", null, [
      [ "ClosedLoop", "classClosed__Loop_1_1ClosedLoop.html", "classClosed__Loop_1_1ClosedLoop" ]
    ] ],
    [ "Closed_Loop7", null, [
      [ "ClosedLoop", "classClosed__Loop7_1_1ClosedLoop.html", "classClosed__Loop7_1_1ClosedLoop" ]
    ] ],
    [ "Closed_loop_task", null, [
      [ "ControllerFSM", "classClosed__loop__task_1_1ControllerFSM.html", null ]
    ] ],
    [ "Closed_loop_task7", null, [
      [ "ControllerFSM", "classClosed__loop__task7_1_1ControllerFSM.html", null ]
    ] ],
    [ "DataCollection", null, [
      [ "collecting", "classDataCollection_1_1collecting.html", null ]
    ] ],
    [ "elevator", null, [
      [ "Button", "classelevator_1_1Button.html", "classelevator_1_1Button" ],
      [ "elevator", "classelevator_1_1elevator.html", null ],
      [ "MotorDriver", "classelevator_1_1MotorDriver.html", "classelevator_1_1MotorDriver" ]
    ] ],
    [ "Encoderclass", null, [
      [ "encoder", "classEncoderclass_1_1encoder.html", "classEncoderclass_1_1encoder" ]
    ] ],
    [ "Encoderclassnew", null, [
      [ "encoder", "classEncoderclassnew_1_1encoder.html", "classEncoderclassnew_1_1encoder" ]
    ] ],
    [ "Encoderclassnew7", null, [
      [ "encoder", "classEncoderclassnew7_1_1encoder.html", "classEncoderclassnew7_1_1encoder" ]
    ] ],
    [ "Encoderclassrevised", null, [
      [ "encoder", "classEncoderclassrevised_1_1encoder.html", "classEncoderclassrevised_1_1encoder" ]
    ] ],
    [ "EncoderFSM", null, [
      [ "EncoderFSM", "classEncoderFSM_1_1EncoderFSM.html", null ]
    ] ],
    [ "Led", null, [
      [ "light", "classLed_1_1light.html", null ]
    ] ],
    [ "Motor_Driver", null, [
      [ "MotorDriver", "classMotor__Driver_1_1MotorDriver.html", "classMotor__Driver_1_1MotorDriver" ]
    ] ],
    [ "Motor_Driver 2", null, [
      [ "MotorDriver", "classMotor__Driver_012_1_1MotorDriver.html", "classMotor__Driver_012_1_1MotorDriver" ]
    ] ],
    [ "Motor_Driver7", null, [
      [ "MotorDriver", "classMotor__Driver7_1_1MotorDriver.html", "classMotor__Driver7_1_1MotorDriver" ]
    ] ],
    [ "UI", null, [
      [ "UI", "classUI_1_1UI.html", null ]
    ] ]
];