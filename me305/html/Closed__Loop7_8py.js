var Closed__Loop7_8py =
[
    [ "ClosedLoop", "classClosed__Loop7_1_1ClosedLoop.html", "classClosed__Loop7_1_1ClosedLoop" ],
    [ "get_Kp", "Closed__Loop7_8py.html#ad51e7c24ccb5cab366271e14e1ed3b59", null ],
    [ "set_Kp", "Closed__Loop7_8py.html#af83196c9f8a3667255d822b06fce58bc", null ],
    [ "update", "Closed__Loop7_8py.html#a25dea7dc959eb87fe6042a88d8544f4c", null ],
    [ "Kp", "Closed__Loop7_8py.html#ab823463c76fdceaa59408be513572e52", null ],
    [ "kpprime", "Closed__Loop7_8py.html#a959d597d7f03f04fb85545b46dd0c3a0", null ],
    [ "L", "Closed__Loop7_8py.html#a9c5eb6412d032eacc029370f0e9d2e12", null ],
    [ "omegaact", "Closed__Loop7_8py.html#a51f0cb7723f2d0710fcad60559516683", null ],
    [ "omegaref", "Closed__Loop7_8py.html#af15ee52de0d9eea5dfc6240f23cc8e3d", null ],
    [ "vm", "Closed__Loop7_8py.html#af85351fba68ba2b0fadca3589f9af448", null ]
];