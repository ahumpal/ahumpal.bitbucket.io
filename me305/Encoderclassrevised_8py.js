var Encoderclassrevised_8py =
[
    [ "encoder", "classEncoderclassrevised_1_1encoder.html", "classEncoderclassrevised_1_1encoder" ],
    [ "getdelta", "Encoderclassrevised_8py.html#aac8d4075d82b1c596eea07fdf611d9f2", null ],
    [ "getposition", "Encoderclassrevised_8py.html#af0dd647701ff3132b75e1bf9603fe126", null ],
    [ "setposition", "Encoderclassrevised_8py.html#a3bdc2c50689c650f548de4d210e8d416", null ],
    [ "update", "Encoderclassrevised_8py.html#abe4804e0b6efb3c5eb424b83cc2edb34", null ],
    [ "A6", "Encoderclassrevised_8py.html#abc7d5c4088042cee80bdc0bff5f1bdbe", null ],
    [ "A7", "Encoderclassrevised_8py.html#ac80517ffd2ed00248307bbde06161353", null ],
    [ "currentposition", "Encoderclassrevised_8py.html#acf6cf58c67c4b8436fc228440f848dd4", null ],
    [ "delta", "Encoderclassrevised_8py.html#ac2cbbf69018a2cba7e579f53c30bfd47", null ],
    [ "deltamag", "Encoderclassrevised_8py.html#accb3cb602929a27e23e6b79fbb883eae", null ],
    [ "mode", "Encoderclassrevised_8py.html#a5f88112a650f9285ff289c2c98b76860", null ],
    [ "period", "Encoderclassrevised_8py.html#a1ffe2b7364f80615176556e020484d86", null ],
    [ "pin", "Encoderclassrevised_8py.html#a0df978b19bbdd48cd4cd285479d05371", null ],
    [ "position", "Encoderclassrevised_8py.html#a46494402b13b91fcf5200269b4faae77", null ],
    [ "prescaler", "Encoderclassrevised_8py.html#aa784a52134d2b2b0956cd71e56cb6cf1", null ],
    [ "previousposition", "Encoderclassrevised_8py.html#ad3ba5873fdca10b0a11f7db0d52a4944", null ],
    [ "tim", "Encoderclassrevised_8py.html#a671a84693a90c78797df5eed0698367f", null ],
    [ "updateddelta", "Encoderclassrevised_8py.html#af5480145c32b89f60fc2091c82b535be", null ]
];