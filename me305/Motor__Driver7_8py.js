var Motor__Driver7_8py =
[
    [ "MotorDriver", "classMotor__Driver7_1_1MotorDriver.html", "classMotor__Driver7_1_1MotorDriver" ],
    [ "disable", "Motor__Driver7_8py.html#a505745196a31958bbdf7762a2f8575e6", null ],
    [ "enable", "Motor__Driver7_8py.html#a5a2f5ac161f631c87bf2eb3708f16630", null ],
    [ "set_duty", "Motor__Driver7_8py.html#a0f33a94f2db3253bb847c5f7c27a5b2a", null ],
    [ "freq", "Motor__Driver7_8py.html#a6bf260ce5a0f7024a0de81c1ed5a82f0", null ],
    [ "IN1_pin", "Motor__Driver7_8py.html#a85a036f20a8250b609560a3b7770a1b7", null ],
    [ "IN2_pin", "Motor__Driver7_8py.html#a87192f8b64b71fb31eaf2e1f11099dc9", null ],
    [ "nSLEEP_pin", "Motor__Driver7_8py.html#a478e9ccaeb748ae1c5a92fe6a1b6bbed", null ],
    [ "pin", "Motor__Driver7_8py.html#ab954304cb65af4436b6853012668d3db", null ],
    [ "pin_1", "Motor__Driver7_8py.html#a2aa673d078392cfd49449c9d7c6c5b20", null ],
    [ "pin_1channel", "Motor__Driver7_8py.html#a429c8a9f0713aec8a470915a7bf3fa08", null ],
    [ "pin_2", "Motor__Driver7_8py.html#ab55422956f28978de3c867b3797329c1", null ],
    [ "pin_2channel", "Motor__Driver7_8py.html#a107dc71a6ef773e96b03eeab7130b189", null ],
    [ "Pinone", "Motor__Driver7_8py.html#ad0d19d4f0f001a91719053e06786b472", null ],
    [ "Pintwo", "Motor__Driver7_8py.html#a2dcb948b6484f952bd31ed2dfb9146d9", null ],
    [ "Powerpin", "Motor__Driver7_8py.html#af2a5afe1acc531a9a366ee6c2194d285", null ],
    [ "PWM", "Motor__Driver7_8py.html#a49283b911fb768fa6fa540f1cc0a42e3", null ],
    [ "tim", "Motor__Driver7_8py.html#a4ead15639254abef9ae3239aa0d7923b", null ],
    [ "timerval", "Motor__Driver7_8py.html#a0aa6c9e21524b5fec7e3efde7d7a3929", null ]
];